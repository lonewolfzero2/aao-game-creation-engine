{
"trial_title" : "Título",
"trial_author" : "Autor",
"trial_collabs" : "Colaboradores",
"trial_playtesters" : "Testers",
"trial_metadata" : "Metadatos",

"play" : "Jugar!",
"edit" : "Editar",

"trial_sequence" : "Parte de",
"trial_language" : "Idioma",
"trial_creation_date" : "Empezado el",
"trial_released" : "Públibo",
"trial_release_date" : "Disponible desde el",
"trial_add_collab" : "Añadir colaborador",
"trial_add_playtester" : "Añadir tester",
"trial_delete": "Borrar caso",
"trial_create": "Crear nuevo caso",

"sequence_title": "Título",
"sequence_metadata": "Metadatos",
"sequence_items": "Elementos de la secuencia",
"sequence_append_trial": "Incluir caso",
"sequence_delete": "Eliminar secuencia",
"sequence_create": "Crear nueva secuencia",

"trial_backups": "Copias de seguridad del caso",
"backups_auto": "Copias automáticas",
"backups_manual": "Copias manuales",
"backups_manual_create": "Guardar versión actual como copia de seguridad"
}
