{
"error_perms_file_edit" : "Impossible de modifier le fichier du procès",
"error_perms_file_delete" : "Impossible de supprimer le fichier du procès",

"error_perms_trial_read" : "Permissions insuffisantes pour lire le procès",
"error_perms_trial_manage_backups" : "Permissions insuffisantes pour gérer les backups du procès",
"error_perms_trial_edit_secured_metadata" : "Permissions insuffisantes pour éditer les métadonnées sécurisées du procès",
"error_perms_trial_edit_metadata" : "Permissions insuffisantes pour éditer les métadonnées du procès",
"error_perms_trial_create_someone_else" : "Permissions insuffisantes pour créer un procès pour un autre utilisateur",

"error_perms_sequence_add_trial" : "Permissions insuffisantes pour ajouter ce procès à une série",
"error_perms_sequence_add_sequence" : "Permissions insuffisantes pour ajouter cette série à une série",
"error_perms_sequence_edit" : "Permissions insuffisantes pour modifier cette série",
"error_perms_sequence_delete" : "Permissions insuffisantes pour supprimer cette série",


"error_user_inactive": "Action disponible uniquement pour les utilisateurs actifs connectés.",

"error_user_trial_language_unavailable" : "La langue choisie n'est pas disponible sur AAO",
"error_user_trial_backup_no_slot" : "Il n'y a plus d'emplacement disponible pour une copie de sauvegarde manuelle de ce procès. Supprimez-en une !",

"error_user_sequence_trial_already_taken" : "Le procès cible fait déjà partie d'une autre série",
"error_user_sequence_sequence_already_taken" : "La série cible est déjà incluse dans une autre série",


"error_tech_v5_trial_language_edit_in_sequence" : "La langue d'un procès faisant partie d'une série ne peut être modifiée",
"error_tech_v5_trial_author_edit_in_sequence" : "L'auteur d'un procès faisant partie d'une série ne peut être modifié.",

"error_tech_v5_sequence_id_already_used" : "Une série avec ce nom existe déjà",
"error_tech_v5_sequence_empty" : "Une série doit contenir au moins un procès",
"error_tech_v5_sequence_incorrect_trial_author" : "Une série ne peut contenir que des procès du même auteur",
"error_tech_v5_sequence_incorrect_trial_language" : "Une série ne peut contenir que des procès dans la même langue",
"error_tech_v5_sequence_nested" : "Les séries imbriquées ne sont pas supportées"
}
