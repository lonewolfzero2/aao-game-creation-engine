{
	"homepage" : "Accueil",
	
	"welcome" : "Bienvenue sur Ace Attorney Online, l'éditeur de procès en ligne !",
	"site_presentation" : "AAO est l'endroit idéal pour jouer, créer et partager des jeux d'aventure basés sur la série Ace Attorney de Capcom.",
	"no_download" : "Pas besoin de télécharger quoi que ce soit : inscrivez-vous et commencez à créer votre propre jeu depuis votre navigateur web immédiatement !",
	
	"news" : "Dernières nouvelles et mises à jour",
	"posted_on" : "Posté le <date> à <time>",
	"view_comments" : "Voir les commentaires",
	
	"random_featured" : "Jeu coup de cœur",
	
	"affiliates" : "Partenaires",
	"contact" : "Contact"
}
