/*
Ace Attorney Online - Characters display engine

*/

//MODULE DESCRIPTOR
Modules.load(new Object({
	name : 'display_engine_characters',
	dependencies : ['display_engine_globals', 'style_loader', 'events', 'objects'],
	init : function() {}
}));

//INDEPENDENT INSTRUCTIONS

//EXPORTED VARIABLES

//EXPORTED FUNCTIONS
function charactersDisplay(render_buffer)
{
	var self = this;
	self.render_buffer = render_buffer || new callbackBuffer();
	
	// DOM structure
	self.render = document.createElement('div'); // Parent element to be included in the page
	addClass(self.render, 'display_engine_characters');
	
	self.currentDefaultPosition = POSITION_CENTER; // Position to insert new unpositioned character. Typically current screen position.
	self.currentPositions = new Object(); // Current positions by ID
	self.charactersById = new Object(); // Character objects present in the scene by character ID
	self.charactersInfoById = new Object(); // Character info by ID
	self.charactersInfoByPosition = new Object(); // Character info by ID of the position occupied on the scene
	self.charactersIsTalkingById = new Object();
	self.charactersIsInStartupById = new Object();
	self.charactersIdInDisplayOrder = new Array(); // Characters ID given in the order in which they are displayed (first is displayed in the back)
	
	// Reset the current positions of the scene to the default ones
	self.resetCurrentPositions = function()
	{
		self.currentPositions = new Object();
		for(var i in default_positions)
		{
			self.currentPositions[default_positions[i].id] = default_positions[i];
		}
	};
	self.resetCurrentPositions();
	
	// Get the current position of a character on the scene; null if character isn't on the scene
	self.getCharacterPosition = function(character_id)
	{
		var character_info = self.charactersInfoById[character_id];
		if(character_info)
		{
			return self.currentPositions[character_info.position];
		}
		else
		{
			return null;
		}
	};
	
	// Set a character (add or move) on the scene; run callback when all information about the character is loaded
	self.setCharacter = function(character_info, transition_enabled, callback)
	{
		// Default pose : talking if manually synced to always talking; still otherwise (will be made to talk if needed by the sync)
		var default_char_pose = character_info.sync_mode == SYNC_TALK ? 'talking' : 'still';
		
		if(!self.charactersById[character_info.profile_id])
		{
			//this character isn't already displayed : create new image object
			var descriptor = getCharacterDescriptor(character_info, default_char_pose);
			
			var newElement = generateGraphicElement(descriptor, function(img, newElement){
				self.render.appendChild(newElement);
				self.setCharacterPosition(character_info, transition_enabled, self.playStartup.bind(self, character_info, callback));
			}, self.render_buffer);
			
			self.charactersById[character_info.profile_id] = newElement;
			self.charactersIdInDisplayOrder.push(character_info.profile_id);
		}
		else
		{
			//character already displayed : load new sprite and move position			
			self.setCharacterStatus(character_info, default_char_pose, function(){ 
				self.setCharacterPosition(character_info, transition_enabled, self.playStartup.bind(self, character_info, callback));
			});
		}
	};
	
	// Trigger transition for appearing character
	self.characterAppears = function(character_info)
	{
		//Check the visual_effect_appears field and act accordingly
	};
	
	// Trigger transition for and delete disappearing character
	self.characterDisappears = function(character_info)
	{
		//Check the visual_effect_disappears field and act accordingly
		
		//Remove element
		var element = self.charactersById[character_info.profile_id];
		self.render_buffer.call(self.render.removeChild.bind(self.render, element));
		
		//Remove character data
		delete self.charactersById[character_info.profile_id];
		delete self.charactersInfoById[character_info.profile_id];
		delete self.charactersInfoByPosition[character_info.position];
		delete self.charactersIsInStartupById[character_info.profile_id];
		delete self.charactersIsTalkingById[character_info.profile_id];
		
		// Remove character from display order list.
		self.charactersIdInDisplayOrder.splice(self.charactersIdInDisplayOrder.indexOf(character_info.profile_id), 1);
	};
	
	// Set the position of a character on screen, and trigger necessary movements of other characters, then callback when all moves are complete.
	self.setCharacterPosition = function(character_info, transition_enabled, callback)
	{
		var element = self.charactersById[character_info.profile_id];
		
		//Set mirror status
		setEffectToGraphicElement(element, 'mirror', character_info.mirror_effect);
		
		//Set source position and transition
		var transition;
		var current_char_info;
		if(current_char_info = self.charactersInfoById[character_info.profile_id])
		{
			//if this character was already displayed
			if(current_char_info.position != character_info.position)
			{
				//And if it actually moves, free its current position
				self.charactersInfoByPosition[current_char_info.position] = null;
				
				//and allow transition to its new position if enabled
				transition = transition_enabled;
			}
		}
		else
		{
			//character enters the scene
			self.characterAppears(character_info);
			//without transition
			transition = false;
		}
		
		//Set destination position
		self.charactersInfoByPosition[character_info.position] = character_info;
		self.charactersInfoById[character_info.profile_id] = character_info;
		
		//Effectively move the character
		var position = self.currentPositions[character_info.position];
		
		// TODO : add option "play frame after move" and "play frame during move", and trigger callback as appropriate for each.
		setGraphicElementPosition(element, position, self.render, transition, callback);
	};
	
	// Remove all displayed characters.
	self.removeCharacters = function()
	{
		for(var i in self.charactersInfoById)
		{
			self.characterDisappears(self.charactersInfoById[i]);
		}
	};
	
	self.removeObsoleteCharacters = function(frame_data)
	{
		for(var i in self.charactersInfoById)
		{
			var is_defined = false; // Character is "defined" if a character entry with the same profile id is present in this frame
			var is_overridden = false; // Character is "overridden" if a character entry with same position is present in this frame
			
			for(var j = 0; !is_defined && j < frame_data.characters.length; j++) // Stop if character is defined - override status does not matter then
			{
				if(frame_data.characters[j].profile_id == i)
				{
					is_defined = true;
				}
				
				if(frame_data.characters[j].position == self.charactersInfoById[i].position)
				{
					is_overridden = true;
				}
			}
			
			if(!is_defined && // A character that is not defined in this frame is erased if
				(frame_data.characters_erase_previous || // All previous characters are erased, or
				is_overridden) // it is overridden 
			)
			{
				self.characterDisappears(self.charactersInfoById[i]);
			}
		}
	};
	
	// Change the animation status of a character on the scene
	self.setCharacterStatus = function(character_info, status, callback)
	{
		var element = self.charactersById[character_info.profile_id];
		
		updateGraphicElement(element, getCharacterDescriptor(character_info, status), callback, self.render_buffer);
	};
	
	// Trigger talking sprite for all synchronised characters
	self.textSyncStart = function(speaker_id)
	{
		for(var i in self.charactersInfoById)
		{
			var char_info = self.charactersInfoById[i];
			if(char_info.sync_mode == SYNC_SYNC || 
				(char_info.sync_mode == SYNC_AUTO && char_info.profile_id == speaker_id))
			{
				if(!self.charactersIsInStartupById[i])
				{
					self.setCharacterStatus(char_info, 'talking');
				}
				self.charactersIsTalkingById[i] = true;
			}
		}
	};
	
	// Trigger still sprite for all synchronised characters
	self.textSyncStop = function()
	{
		for(var i in self.charactersInfoById)
		{
			var char_info = self.charactersInfoById[i];
			if(char_info.sync_mode == SYNC_SYNC || char_info.sync_mode == SYNC_AUTO)
			{
				if(!self.charactersIsInStartupById[i])
				{
					self.setCharacterStatus(char_info, 'still');
				}
				self.charactersIsTalkingById[i] = false;
			}
		}
	};
	
	self.playStartup = function(character_info, callback)
	{
		// Restores the synchronisation of the character after the startup annimation
		function resumeSync(character_info)
		{
			self.charactersIsInStartupById[character_info.profile_id] = false;
			
			switch(character_info.sync_mode)
			{
				case SYNC_SYNC:
				case SYNC_AUTO:
					
					if(self.charactersIsTalkingById[character_info.profile_id])
					{
						self.setCharacterStatus(character_info, 'talking');
					}
					else
					{
						self.setCharacterStatus(character_info, 'still');
					}
				
					break;
				
				case SYNC_TALK:
					self.setCharacterStatus(character_info, 'talking');
					break;
				
				case SYNC_STILL:
					self.setCharacterStatus(character_info, 'still');
					break;
			}
		}
		
		var pose_desc = getPoseDesc(character_info);
		
		if(character_info.startup_mode == STARTUP_SKIP
			|| pose_desc.startup_duration == 0 )
		{
			// In this mode, do not play startup and run callback immediately
			if(callback)
			{
				callback();
			}
		}
		else
		{
			// Launch startup sprite
			self.charactersIsInStartupById[character_info.profile_id] = true;
			self.setCharacterStatus(character_info, 'startup');
			
			if(character_info.startup_mode == STARTUP_PLAY_BEFORE)
			{
				// In this mode, delay callback to after the startup animation is complete
				window.setTimeout(function(){
					resumeSync(character_info);
					if(callback)
					{
						callback();
					}
				}, pose_desc.startup_duration);
			}
			else
			{
				// In this mode, run callback immediately
				window.setTimeout(function(){
					resumeSync(character_info);
				}, pose_desc.startup_duration);
				
				if(callback)
				{
					callback();
				}
			}
		}
	};
	
	// Get the information about the frame's characters
	self.getCharactersInfo = function(frame_characters)
	{
		var characters = new Array();
		
		for(var i = 0; i < frame_characters.length; i++)
		{
			//Clone character info to avoid side effects from other functions
			var character_info = new objClone(frame_characters[i]);
			var to_be_pushed = true; // Determine if character actually has to be displayed on screen
			
			// Handle unpositioned characters
			if(character_info.position == POSITION_NONE)
			{
				//if position is none
				if(self.charactersInfoById[character_info.profile_id])
				{
					//and character was already displayed, set position to the same
					character_info.position = self.charactersInfoById[character_info.profile_id].position;
				}
				else
				{
					// New unpositioned character : shouldn't happen in V6 games, but frequent in legacy trials
					// Set character to current screen position
					character_info.position = self.currentDefaultPosition;
				}
			}
			
			// Handle characters with no sprite set
			if(character_info.sprite_id == 0)
			{
				if(self.charactersInfoById[character_info.profile_id])
				{
					//If character was already displayed, set sprite to the same
					character_info.sprite_id = self.charactersInfoById[character_info.profile_id].sprite_id;
				}
				else
				{
					//Otherwise, it won't be displayed : remove from the list
					to_be_pushed = false;
				}
			}
			
			// Add character to list if needed
			if(to_be_pushed)
			{
				characters.push(character_info);
			}
		}
		
		return characters;
	};
	
	// Load characters from frame data
	self.loadFrameCharacters = function(frame_data, transition_enabled, callback)
	{
		// If there is a place set, update the positions list
		if(frame_data.place)
		{
			self.resetCurrentPositions();
			var place_data = getPlace(frame_data.place);
			for(var i = 0; i = place_data.positions.length; i++)
			{
				self.currentPositions[place_data.positions[i].id] = place_data.positions[i];
			}
			
			// TODO : decide what to do if characters haven't been cleared - should characters on a place-specific position be cleared anyway ?
		}
		
		// Get characters information
		var characters = self.getCharactersInfo(frame_data.characters); 
		
		// Remove characters that become obsolete in this frame
		self.removeObsoleteCharacters(frame_data);
		
		var loaded_characters = -1; // -1, so that we can trigger the function immediately after
		var checkAndRunCallback = callback ? (function() {
			if(++loaded_characters == characters.length)
			{
				// If all characters are loaded, run callback
				callback();
			}
		}) : null;
		if(checkAndRunCallback)
		{
			checkAndRunCallback(); // Trigger the function, even if there is no character
		}
		
		//load all characters
		for(var i = 0; i < characters.length; i++)
		{
			self.setCharacter(characters[i], transition_enabled, checkAndRunCallback);
		}
	};
	
	// Export and restore state of characters display engine.
	Object.defineProperty(self, 'state', {
		get: function()
		{
			return {
				currentDefaultPosition: self.currentDefaultPosition,
				currentPositions: self.currentPositions,
				characters: self.charactersInfoById,
				characters_order: self.charactersIdInDisplayOrder
			};
		},
		set: function(state)
		{
			self.removeCharacters();
			
			self.currentDefaultPosition = state.currentDefaultPosition;
			self.currentPositions = state.currentPositions;
			
			for(var i = 0; i < state.characters_order.length; i++)
			{
				self.setCharacter(state.characters[state.characters_order[i]], false);
			}
		}
	});
}

//END OF MODULE
Modules.complete('display_engine_characters');
